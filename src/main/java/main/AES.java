 /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package main;

import java.nio.charset.StandardCharsets;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * En aquest exemple es treballa amb el xifrat simètric.
 * - Es xifra un missatge usant AES i una clau 
 * - Es desxifra el mateix missatge de nou al seu estat inicial 
 * 
 * https://www.baeldung.com/java-aes-encryption-decryption
 * 
 * @author manel
 */
public class AES {
    
    private static final Logger logger = LogManager.getLogger(AES.class);

    public static void main(String[] args) {
        
        try {
            
            //password introduït per l'usuari (entre 8 i 24 caràcters)
            String password = "12345678";
            
            // usem l'algoritme PBKDF2WithHmacSHA256 per a generar una clau secreta mitjançant un password
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            
            // SALT: valor aleatori utilitzat per fer única cada codificació. 
            // Aquest valor s'emmagatzema en la pròpia codificació per a recuperar-lo de nou en la
            // decodificació
            String salt = "abcdefg";
            
            // KeySpec = clau criptogràfica
            // 128: nombre d'iteracions de la funció hash
            // 128: Longitud de la clau derivada en bits
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(StandardCharsets.UTF_8), 128, 128);
            SecretKey secret = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
            logger.info("Clau (base64): " +Base64.getEncoder().encodeToString(secret.getEncoded()));

            //Vector d'inicialització de 128 bits
            byte[] iv = new byte[16];
            new SecureRandom().nextBytes(iv);
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            
            // missatge a xifrar
            String m = "L'experiència és una cosa meravellosa. Et permet reconèixer un error quan el tornes a cometre.";
            logger.info("Missatge:" + m);
            byte[] secretMessagesBytes = m.getBytes(StandardCharsets.UTF_8);
            
            // Els diferents algoritmes/modes/paddings permesos esyan explicats a: https://docs.oracle.com/en/java/javase/17/docs/specs/security/standard-names.html#cipher-algorithm-names
            Cipher transformar = Cipher.getInstance("AES/CBC/PKCS5Padding");
            AlgorithmParameters params = transformar.getParameters();
            logger.info("Algoritme:" + params.getAlgorithm());
            transformar.init(Cipher.ENCRYPT_MODE, secret, ivSpec);
            
            //missatge xifrat
            byte[] encryptedMessageBytes = transformar.doFinal(secretMessagesBytes);
            String base64Encoded = Base64.getEncoder().encodeToString(encryptedMessageBytes);
            logger.info("Missatge xifrat (base64):" + base64Encoded);
            
            //desxifrat
            transformar.init(Cipher.DECRYPT_MODE, secret, ivSpec);
            byte[] decryptedMessageBytes = transformar.doFinal(Base64.getDecoder().decode(base64Encoded));
            logger.info("Missatge desxifrat:" + new String(decryptedMessageBytes, StandardCharsets.UTF_8));
            
            
        } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
            logger.error("Error en l'algorisme: " + noSuchAlgorithmException);
        } catch (NoSuchPaddingException noSuchPaddingException) {
            logger.error("Error en el padding: " + noSuchPaddingException);
        } catch (InvalidKeyException invalidKeyException) {
            logger.error("Error en la clau: " + invalidKeyException);
        } catch (InvalidAlgorithmParameterException invalidAlgorithmParameterException) {
            logger.error("Error en els paràmetres de l'algorisme: " + invalidAlgorithmParameterException);
        } catch (IllegalBlockSizeException ex) {
            logger.error("Error en el tamany de bloc: " + ex);
        } catch (BadPaddingException ex) {
            logger.error("Padding incorrecte: " + ex);
        } catch (InvalidKeySpecException ex) {
            logger.error("Error en la clau generada: " + ex);
        }
    }
}
