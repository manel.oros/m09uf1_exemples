/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package main;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.bouncycastle.util.io.pem.PemWriter;

/**
 * En aquest exemple es treballa amb el xifrat asimètric:
 * 
 * - es generen un parell de claus RSA de 2048 bits
 * - es desen en dos fixters en format PEM
 * - es tornen a llegir del disc
 * - es mostren les seves característiques
 * @author manel
 */
public class RSA {
    
    private static final Logger logger = LogManager.getLogger(RSA.class);

    public static void main(String[] args) {
        try {
                       
            logger.info("Definim noms de fitxers...");
            String homeDir = System.getProperty("user.home");
            Path privateKeyFile = Paths.get(homeDir,"private_key.pem"); 
            Path publicKeyFile = Paths.get(homeDir,"public_key.pem"); 
            
            logger.info("Inicialitzem generador RSA 2048 bits...");
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            
            logger.info("Generem parell de claus...");
            PrivateKey privateKey = keyPair.getPrivate();
            PublicKey publicKey = keyPair.getPublic();
            
            logger.info("Desem clau publica al fitxer "+ publicKeyFile + " utilitzant la classe PemWriter de la llibreria bcprov-jdk15on de BouncingCastle");
            try (FileWriter fileWriter = new FileWriter(publicKeyFile.toString()); PemWriter pemWriter = new PemWriter(fileWriter)) {
                pemWriter.writeObject(new PemObject("PUBLIC KEY", publicKey.getEncoded()));
                pemWriter.flush();
                pemWriter.close();
            }
            
            logger.info("Desem clau privada al fitxer "+ privateKeyFile + " utilitzant la classe PemWriter de la llibreria bcprov-jdk15on de BouncingCastle");
            try (FileWriter fileWriter = new FileWriter(privateKeyFile.toString()); PemWriter pemWriter = new PemWriter(fileWriter)) {
                pemWriter.writeObject(new PemObject("PRIVATE KEY", privateKey.getEncoded()));
                pemWriter.flush();
                pemWriter.close();
            }
            
            logger.info("Recuperem dades del fixter " + publicKeyFile + " utilitzant la classe PemReader de la llibreria bcprov-jdk15on de BouncingCastle");    
            RSAPublicKey rsaPublicKey;
            byte[] content;
            try (FileReader fileReader = new FileReader(publicKeyFile.toString());
                    
                PemReader pemReader = new PemReader(fileReader)) {
                PemObject pemObject = pemReader.readPemObject();
                content = pemObject.getContent();
            }
            
            logger.info("Formatem dades a X509");
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(content);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            rsaPublicKey = (RSAPublicKey)keyFactory.generatePublic(publicKeySpec);
            
            logger.info("Mostrem dades de la clau publica");
            System.out.println();
            System.out.println("CLAU PUBLICA:");
            System.out.println("===============================================================");
            System.out.println("Public Key Format: " + rsaPublicKey.getFormat());
            System.out.println("Decimal modulus (n): " + rsaPublicKey.getModulus());
            System.out.println("Decimal modulus length in digits: " + rsaPublicKey.getModulus().toString().length());
            System.out.println("Binary modulus (n): " + rsaPublicKey.getModulus().toString(2));
            System.out.println("Binary modulus length in digits: " + rsaPublicKey.getModulus().toString(2).length());
            System.out.println("Public Exponent (e): " + rsaPublicKey.getPublicExponent());
            System.out.println("===============================================================");
            
            logger.info("Recuperem dades de fixter " +  privateKeyFile+ " utilitzant la classe PemReader de la llibreria bcprov-jdk15on de BouncingCastle");        
            java.security.interfaces.RSAPrivateKey rsaPrivateKey;
            try (FileReader fileReader = new FileReader(privateKeyFile.toString());
                PemReader pemReader = new PemReader(fileReader)) {
                PemObject pemObject = pemReader.readPemObject();
                content = pemObject.getContent();
            }
            
            logger.info("Formatem dades a PKCS#8");
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(content);
            keyFactory = KeyFactory.getInstance("RSA");
            rsaPrivateKey = (RSAPrivateKey)keyFactory.generatePrivate(privateKeySpec);
            
            logger.info("Mostrem dades de la clau privada...");
            System.out.println();
            System.out.println("CLAU PRIVADA:");
            System.out.println("===============================================================");              
            System.out.println("Private Key Format: " + rsaPrivateKey.getFormat());
            System.out.println("Private exponent (j): " + rsaPrivateKey.getPrivateExponent());
            System.out.println("Algorithm: " + rsaPrivateKey.getAlgorithm());
            System.out.println("===============================================================");
            
            
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException ex) {
           logger.info("Error: {0}", ex.toString());
        }
    }
}
