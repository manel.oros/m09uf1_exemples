/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * En aquest exemple es pot veure una implementació del algorisme de resum MD5 i la seva comparació 
 * amb la implementació de java.security.MessageDigest. 
 * 
 * El resum resultant sempre ha de ser el mateix, independentment de la implementació escollida.
 * 
 * @author manel extret de https://rosettacode.org/wiki/MD5/Implementation
 * 
 * Segons la RFC 1321 (https://www.rfc-es.org/rfc/rfc1321-es.txt):
 * Este documento describe el algoritmo de resumen de mensajes MD5. El
   algoritmo toma como entrada un mensaje de longitud arbitraria y
   produce como salida una "huella" de 128 bits o "resumen del mensaje"
   de entrada. Se conjetura que es computacionalmente inviable encontrar
   dos mensajes que tengan el mismo resumen, u obtener un mensaje que
   tenga un resumen de mensaje en concreto, establecido previamente como
   objetivo. El algoritmo MD5 está pensado para ser aplicado en firmas
   digitales, donde un archivo grande debe ser "comprimido" de forma
   segura antes de ser cifrado con una llave privada (secreta) dentro de
   un sistema de cifrado de llave pública, como RSA.  El algoritmo MD5
   ha sido diseñado para ser bastante rápido en máquinas de 32 bits.
   Además, no requiere de ninguna gran tabla de sustitución; puede ser
   codificado de forma bastante compacta.
 * 
 */
public class MD5 {
    
    private static final Logger logger = LogManager.getLogger(MD5.class);
    
    // inicialització del buffer MD (similar al vector d'inicialització)
    // 4 registres A,B,C i D amb els valors predefinits
    // expressats en little endian (els de menor pes van primer):
    //
    //A: 01 23 45 67
    //B: 89 ab cd ef
    //C: fe dc ba 98
    //D: 76 54 32 10
    private static final int INIT_A = 0x67452301;
    private static final int INIT_B = 0xEFCDAB89;
    private static final int INIT_C = 0x98BADCFE;
    private static final int INIT_D = 0x10325476;

    
    private static final int[] SHIFT_AMTS = {
      7, 12, 17, 22,
      5,  9, 14, 20,
      4, 11, 16, 23,
      6, 10, 15, 21
    };

    //prepara array de 64 posicions de tipus enter
    private static final int[] TABLE_T = new int[64];
    
    //static: forma d'executar un fragment de codi desvinculat d'un mètode concret (similar a javascript)
    static
    {
      //s'omple taula de valors pseudoaleatoris (deterministes)
      for (int i = 0; i < 64; i++)
        TABLE_T[i] = (int)(long)((1L << 32) * Math.abs(Math.sin(i + 1)));
    }

    /***
     * Implementació del cálcul
     * @param message
     * @return 
     */
    public static byte[] computeMD5(byte[] message)
    {
      int messageLenBytes = message.length;
      
      // nombre de blocs de 64 bits que es necessiten (>>>6 és dividir entre 64) 
      // és el resultat d'augmentar en 8 bytes la lingitud, dividir entre 64 i sumar-li un bloc més
      // si la longitud és entre 0 i 64, el nombre de blocs és 1
      // si la longitud = 65, el nombre de blocs és 2
      // si la longitud = 128, el nombre de blocs és 2
      // si la longitud = 129, el nombre de blocs és 3
      // etc...
      int numBlocks = ((messageLenBytes + 8) >>> 6) + 1;
      
      //la longitud total del missatge queda adaptada al nombre de blocs (<<6 és multiplicar per 64)
      int totalLen = numBlocks << 6;
      
      //array amb els bytes de farciment 
      byte[] paddingBytes = new byte[totalLen - messageLenBytes];
      
      //primer byte del farciment
      paddingBytes[0] = (byte)0x80;

      //longitud total del missatge en bits = longitud en bytes*8 (<<3)
      long messageLenBits = (long)messageLenBytes << 3;
      
      // Padding:
      // els darrers 64 bits (8 bytes) del padding,
      // s'omplen amb els primers 8 bits de la lingutud del missatge
      for (int i = 0; i < 8; i++)
      {
        paddingBytes[paddingBytes.length - 8 + i] = (byte)messageLenBits;
        messageLenBits >>>= 8;
      }

      //inicialitzem valors a,b,c,d de 32 bita cadascún, que formaran la paraula de sortida de 128 bits
      int a = INIT_A;
      int b = INIT_B;
      int c = INIT_C;
      int d = INIT_D;
      
      //preparem un buffer de 64 bytes (16 posicions de 32 bits cada posició).
      int[] buffer = new int[16];
      
      // per cada bloc del missatge
      for (int i = 0; i < numBlocks; i ++)
      {
        //index de salt de 64 en 64 posicions  (i << 6) = i*2^6 = i*64
        int index = i << 6;
        
        // omplim les posicions del buffer amb el resultat del càlcul de les posicions del missatge amb les posicoins anteriors d'aquest
        for (int j = 0; j < 64; j++, index++)
        { 
            // si no hem arribat al final del missatge, retorna la posició corresponent del missatge. Si no, selecciona un valor del padding.
            // Amb el valor obtingut realitza una operació OR binaria amb el valor anterior de la posició del buffer, però desplaçant els seus bits 8 posicions a la dreta
            buffer[j >>> 2] = ((int)((index < messageLenBytes) ? message[index] : paddingBytes[index - messageLenBytes]) << 24) | (buffer[j >>> 2] >>> 8);
        }
        
        int originalA = a;
        int originalB = b;
        int originalC = c;
        int originalD = d;
        
        //recorrem el buffer, ara ja amb valors
        for (int j = 0; j < 64; j++)
        {
          
          int div16 = j >>> 4;
          int f = 0;
          int bufferIndex = j;
          
          // anem assignant a f un detarmiknat valor segons la funció a,b,c o d de forma seqüencial
          switch (div16)
          {
            case 0:
              f = (b & c) | (~b & d);
              break;

            case 1:
              f = (b & d) | (c & ~d);
              bufferIndex = (bufferIndex * 5 + 1) & 0x0F;
              break;

            case 2:
              f = b ^ c ^ d;
              bufferIndex = (bufferIndex * 3 + 5) & 0x0F;
              break;

            case 3:
              f = c ^ (b | ~d);
              bufferIndex = (bufferIndex * 7) & 0x0F;
              break;
          }
          
          //apliquem el càlcul a la posició del buffer corresponent, on està implicada la taula pseudoaleatoria 
          //generada al principi
          int temp = b + Integer.rotateLeft(a + f + buffer[bufferIndex] + TABLE_T[j], SHIFT_AMTS[(div16 << 2) | (j & 3)]);
          
          //intercanviem valors
          a = d;
          d = c;
          c = b;
          b = temp;
        }

        //acumulem als valors a,b,c,d de 32 bita cadascún, que formaran la paraula de sortida de 128 bits
        a += originalA;
        b += originalB;
        c += originalC;
        d += originalD;
      }

      //array resultat de 128 bits (16*8) expressat en little endian
      byte[] md5 = new byte[16];
      int count = 0;
      
      
      for (int i = 0; i < 4; i++)
      {
        int n = (i == 0) ? a : ((i == 1) ? b : ((i == 2) ? c : d));
        for (int j = 0; j < 4; j++)
        {
          md5[count++] = (byte)n;
          n >>>= 8;
        }
      }
      return md5;
    }

    public static String toHexString(byte[] b)
    {
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < b.length; i++)
      {
        sb.append(String.format("%02x", b[i] & 0xFF));
      }
      return sb.toString();
    }
}
