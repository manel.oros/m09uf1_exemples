/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package main;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * En aquest exemple es treballa el protocol TLS:
 *  
 * La primera realitza una petició-resposta HTTP de forma insegura, amb un socket convencional.
 * La segona realitza una petició-resposta HTTP de forma segura, amb un socket TLS.
 * 
 * Obrir Wireshark i capturar els paquets durant l'execució. Veure amb el filtre: tcp.port == 80 or tcp.port == 443
 * 
 * @author manel w/ChatGPT
 */
public class TLS {
    
    private static SSLContext sslContext;
    private static final Logger logger = LogManager.getLogger(AES.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String dades;
        String host = "example.com";
        
        SSLSocket socketSegur;
        BufferedReader nodeRemotSegur;
        OutputStream nodeLocalSegur;
        
        Socket socketInsegur;
        BufferedReader nodeRemot;
        OutputStream nodeLocal;
        
        try {
            
            logger.info("Iniciem comunicació NO segura amb "+host);
            
            socketInsegur = new Socket(host, 80);
            
            // Inicialitzem un socket insegur
            nodeRemot = new BufferedReader(new InputStreamReader(socketInsegur.getInputStream()));
            nodeLocal = socketInsegur.getOutputStream();
            
             // Enviar datos al servidor
            nodeLocal.write("GET / HTTP/1.1\r\nHost: example.com\r\n\r\nRange: bytes=100-200\r\n\r\n".getBytes());
            nodeLocal.flush();

            // Obtenim resposta del servidor
            while ((dades = nodeRemot.readLine()) != null) {
                //System.out.println(dades);
            }
            
            nodeRemot.close();
            nodeLocal.close();
            socketInsegur.close();
            
            logger.info("Finalitzem comunicació NO segura.....");
            logger.info("Prem una tecla per continuar....");
            System.in.read();
            logger.info("===========================================================================");
            logger.info("Iniciem comunicació segura amb "+host);
            
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, null, null);
            
            // Inicialitzem un socket SSL
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            socketSegur = (SSLSocket) sslSocketFactory.createSocket(host, 443);

            // Obtenir buffer e/s
            nodeRemotSegur = new BufferedReader(new InputStreamReader(socketSegur.getInputStream()));
            nodeLocalSegur = socketSegur.getOutputStream();

            // Enviem una petició GET al servidor
            nodeLocalSegur.write("GET / HTTP/1.1\r\nHost: example.com\r\n\r\nRange: bytes=100-200\r\n\r\n".getBytes());
            nodeLocalSegur.flush();

            // Obtenim resposta del servidor
            while ((dades = nodeRemotSegur.readLine()) != null) {
                //System.out.println(dades);
            }

            // Tanquem connexió
            nodeRemotSegur.close();
            nodeLocalSegur.close();
            socketSegur.close();
            
            logger.info("Finalitzem comunicació segura.....");
            
            
        } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
            logger.error("Error en l'algorisme: " + noSuchAlgorithmException);
        } catch (KeyManagementException ex) {
            logger.error("Error: " + ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(TLS.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
