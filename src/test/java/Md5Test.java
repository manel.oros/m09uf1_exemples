/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.Instant;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.MD5;
import static main.MD5.computeMD5;
import static main.MD5.toHexString;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 *
 * @author manel
 */
public class Md5Test {
    
    public Md5Test() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    /***
     * Verifica el correcte funcionament de laimplementació del MD5 amb cadenes curtes
     */
    @Test
    public void cadenesCurtes() {
        try {
            String[] entrades = { "No hacer nada es difícil, nunca se sabe cuando terminas", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "El Pony Pisador: El Meu Pare és un Formatge","a","b"};
            String md5Implementat;
            StringBuilder md5JavaSecurity;
            
            
            
            //inicialitzem generador de MD5 de java.security.MessageDigest
            MessageDigest md = MessageDigest.getInstance("MD5");
            
            for (String testString : entrades) {
                
                //obtenim el MD5 de la nostra funció
                md5Implementat = toHexString(computeMD5(testString.getBytes()));
                
                //obtenim un MD5 de java.security.MessageDigest
                md.update(testString.getBytes());
                byte[] digest = md.digest();
                md5JavaSecurity = new StringBuilder();
                for (byte b : digest) {
                    md5JavaSecurity.append(String.format("%02x", b));
                }
                
                //verifica si el MD5 calculat amb la nostra implementació coincideix amb el MD5 de java.security.MessageDigest (SEMPRE HAURIA DE COINCIDIR !!!!)
                assertEquals(md5Implementat, md5JavaSecurity.toString());
                        
                System.out.println("Text: " + testString + System.lineSeparator() + 
                        "Md5Implementat : "+ md5Implementat + System.lineSeparator() +
                        "md5JavaSecurity: "+ md5JavaSecurity + System.lineSeparator()
                );
                
            }
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("Error: " + ex.toString());
        }
     }
    
    @Test
    public void testLlarg() {
        
        try {
            String md5Implementat;
            StringBuilder md5JavaSecurity;
            
            //inicialitzem generador de MD5 de java.security.MessageDigest
            MessageDigest md = MessageDigest.getInstance("MD5");
            
            //obtenim els bytes del llibre
            byte[] llibre = Files.readAllBytes(Paths.get(MD5.class.getClassLoader().getResource("mobydick.txt").toURI()));
            
            //inciem crono
            Instant start = Instant.now();
            
            //obtenim un MD5 de java.security.MessageDigest
            // TODO: Codi duplicat!! Fer un mètode.
            md.update(llibre);
            byte[] digest = md.digest();
            md5JavaSecurity = new StringBuilder();
            for (byte b : digest) {
                md5JavaSecurity.append(String.format("%02x", b));
            }
            
            //parem crono
            Instant finish = Instant.now();
            System.out.println("Algorisme md5JavaSecurity: " + md5JavaSecurity + " = " + Duration.between(start, finish).toMillis() + " milis");
            
            //inciem crono
            start = Instant.now();
            
            //obtenim el MD5 de la nostra funció
            md5Implementat = toHexString(computeMD5(llibre));
            
            //parem crono
            finish = Instant.now();
            System.out.println("Algorisme md5Implementat : " + md5Implementat + " = " + Duration.between(start, finish).toMillis() + " milis");
            
            //verifica si el MD5 calculat amb la nostra implementació coincideix amb el MD5 de java.security.MessageDigest (SEMPRE HAURIA DE COINCIDIR !!!!)
            assertEquals(md5Implementat, md5JavaSecurity.toString());
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("No es troba l'algorisme " + ex);
        } catch (IOException ex) {
            System.out.println("Error amb la lectura del fitxer" + ex);
        } catch (URISyntaxException ex) {
            System.out.println("Error amb la ruta del fitxer" + ex);
        }
    
    }
}
